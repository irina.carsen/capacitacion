# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 11:10:23 2021

@author: irica
"""

import requests
import time

def get_vendor(mac_address):
    url = "https://api.macvendors.com/"+mac_address
    r = requests.get(url)
    if r.status_code == 200:
        return r.text
    else:
        return r.status_code


hosts = {
"192.168.0.4" : {"mac":"00:02:17:43:65:26"},
"192.168.0.5" : {"mac":"14:5f:94:32:89:47"},
"192.168.0.6" : {"mac":"00:14:F6:45:34:54"}
}

for host, mac in hosts.items():
    vendor = get_vendor(mac["mac"])
    #print(vendor)
    mac.update({"vendor": vendor})
    time.sleep(1)
    
print(hosts)
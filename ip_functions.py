def print_ip_bytes(ip):

    ip_list = ip.split(".")

    l=len(ip_list)

    byte_int = 0;

    if (l!=4):
        print("Error, there must be four bytes in the ip address")
    else: 
        for byte in ip_list:
            byte_int = int(byte)
            if (byte_int>255):
                print("Error, byte cannot be greater than 255")
            else:                
                print(byte)

import json
# open json file and load its content to the variable data
with open("l3vpn.json") as json_file:
	data = json.load(json_file)
l = data['l3vpn']


for customer in l:
    if(customer['Status']=='0'):
        print("Enable VPN")
    elif (customer['Status']=='1'):
        print("Disable VPN")
    else:
        print("Delete VPN")
    print(customer['CustomerName'])
